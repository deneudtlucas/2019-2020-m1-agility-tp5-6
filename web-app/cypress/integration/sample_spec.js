// Test d'exemple par défaut :
describe('The Traffic web site home page', () => {
    it('successfully loads', () => {
        cy.visit('/')
    })
})

// Test d'accès à la page "Configuration"
describe('The Traffic web site configuration page', () => {
    it('successfully loads', () => {
        cy.get('a[href="#configuration"]').click();
        cy.url().should('include', '/#configuration')
    })
})

// Test du nombre de segments
describe('The Traffic web site segments number', () => {
    it('successfully founds 28 segments', () => {
        // Recherche des <tr> du tableau des segments
        cy.get('.col-md-4 tbody').find('tr').should('have.length', 28)
    })
})

// Test du nombre de véhicules
describe('The Traffic web site actual vehicle number', () => {
    it('successfully founds 0 vehicle', () => {
        // Recherche du <td> du tableau des véhicules
        cy.get('.col-md-8 tbody tr').find('td').first().should('contain', 'No vehicle available')
    })
})

// Test de la modification d'un segment
describe('The Traffic web site segment modification', () => {
    it('successfully modified segment 5', () => {
        cy.get('.col-md-4 tbody').find('tr').eq(4).find('th').eq(1).find('input').clear().type('30')
        cy.get('.col-md-4 tbody').find('tr').eq(4).find('th').eq(2).find('button').click()
    })
    it('successfully closed popup', () => {
        cy.get('body .modal-content').find('.modal-body').should('contain', 'Element has been updated')
        cy.get('.modal-footer').find('button').click()

        cy.get('body').not('.modal-content')
    })
    it('successfully modified segment 5 on elements', () => {
        cy.request('http://127.0.0.1:4567/elements').should((response) => {
            expect(response.body['segments'][4]).to.have.property('speed', 30)
        })
    })
})

// Test de modification du rond point
describe('The Traffic web site Roundabout modification', () => {
    it('successfully modified Roundabout', () => {
        cy.get('.card-deck').find('.card').eq(2).find('input[name="capacity"]').clear().type('4')
        cy.get('.card-deck').find('.card').eq(2).find('input[name="duration"]').clear().type('15')
        cy.get('.card-deck').find('.card').eq(2).find('button').click()

        cy.get('.modal-footer').find('button').click()
    })
    it('successfully modified segment 5 on client', () => {
        cy.get('a[href="#configuration"]').click();

        cy.get('.card-deck').find('.card').eq(2).find('input[name="capacity"]').should('have.value', '4')
        cy.get('.card-deck').find('.card').eq(2).find('input[name="duration"]').should('have.value', '15')
    })
    it('successfully modified segment 5 on elements', () => {
        cy.request('http://127.0.0.1:4567/elements').should((response) => {
            expect(response.body['crossroads'][2]).to.have.property('capacity', 4)
            expect(response.body['crossroads'][2]).to.have.property('duration', 15)
        })
    })
})

// Test de modification du feux d'identifiant 29
describe('The Traffic web site crossroad with id 29', () => {
    it('successfully modified crossroad with id 29', () => {
        cy.get('.card-deck').find('.card').eq(0).find('input[name="orangeDuration"]').clear().type('4')
        cy.get('.card-deck').find('.card').eq(0).find('input[name="greenDuration"]').clear().type('40')
        cy.get('.card-deck').find('.card').eq(0).find('input[name="nextPassageDuration"]').clear().type('8')
        cy.get('.card-deck').find('.card').eq(0).find('button').click()

        cy.get('.modal-footer').find('button').click()
    })
    it('successfully modified crossroad with id 29 on elements', () => {
        cy.request('http://127.0.0.1:4567/elements').should((response) => {
            expect(response.body['crossroads'][0]).to.have.property('orangeDuration', 4)
            expect(response.body['crossroads'][0]).to.have.property('greenDuration', 40)
            expect(response.body['crossroads'][0]).to.have.property('nextPassageDuration', 8)
        })
    })
})

// Test d'ajout de véhicules
describe('The Traffic web site add vehicles', () => {
    it('successfully added vehicles', () => {
        addVehicles()
    })
    it('successfully retrieves', () => {
        cy.request('http://127.0.0.1:4567/vehicles').should((response) => {
            expect(response.body['50.0'])
            expect(response.body['200.0'])
            expect(response.body['150.0'])
        })
    })
})

// Test, réalisation d'une simulation
describe('The Traffic web site make a simulation of 120 seconds', () => {
    it('successfully checked vehicles states at start', () => {
        cy.get('a[href="#simulation"]').click();

        cy.get('.col-md-7 table').find('tr').within(() => {
            cy.get('th').eq(5).get('span').should('contain', 'block');
        })
    })
    it('successfully made simulation', () => {
        cy.get('form[name="runNetwork"]').find('input[name="time"]').clear().type('120')
        cy.get('form[name="runNetwork"]').find('button').click()

        cy.get('.col-md-2 div.progress .progress-bar', { timeout: 30000 }).should('have.attr', 'aria-valuenow','100')
    })
    it('successfully checked vehicles states at end', () => {
        cy.get('.col-md-7 table').find('tr').eq(0).get('span').should('contain', 'block')
        cy.get('.col-md-7 table').find('tr').eq(1).get('span').should('contain', 'block')
        cy.get('.col-md-7 table').find('tr').eq(2).get('span').should('contain', 'play_circle_filled')

    })
})

// Test, réalisation d'une simulation
describe('The Traffic web site make a simulation of 500 seconds', () => {
    it('successfully reloaded simulation', () => {
        cy.reload().get('a[href="#configuration"]').click();
        cy.get('.col-md-8 tbody tr').find('td').first().should('contain', 'No vehicle available')
    })
    it('successfully added vehicles', () => {
        addVehicles()
    })
    it('successfully made simulation', () => {
        cy.get('a[href="#simulation"]').click();

        cy.get('form[name="runNetwork"]').find('input[name="time"]').clear().type('500')
        cy.get('form[name="runNetwork"]').find('button').click()

        cy.get('.col-md-2 div.progress .progress-bar', { timeout: 120000 }).should('have.attr', 'aria-valuenow','100')
    })
    it('successfully checked vehicles states at end', () => {
        cy.get('.col-md-7 table').find('tr').eq(0).get('span').should('contain', 'block')
        cy.get('.col-md-7 table').find('tr').eq(1).get('span').should('contain', 'block')
        cy.get('.col-md-7 table').find('tr').eq(2).get('span').should('contain', 'block')
    })
})

// Test, réalisation d'une simulation
describe('The Traffic web site make a simulation of 200 seconds', () => {
    it('successfully reloaded simulation', () => {
        cy.reload().get('a[href="#configuration"]').click();
        cy.get('.col-md-8 tbody tr').find('td').first().should('contain', 'No vehicle available')
    })
    it('successfully added vehicles', () => {
        addVehicles2()
    })
    it('successfully made simulation', () => {
        cy.get('a[href="#simulation"]').click();

        cy.get('form[name="runNetwork"]').find('input[name="time"]').clear().type('200')
        cy.get('form[name="runNetwork"]').find('button').click()

        cy.get('.col-md-2 div.progress .progress-bar', { timeout: 60000 }).should('have.attr', 'aria-valuenow','100')
    })
    it('successfully checked vehicles states at end', () => {
        cy.get('.col-md-7 tbody').find('tr').eq(0).find('th').eq(4).should('contain', '29')
        cy.get('.col-md-7 tbody').find('tr').eq(1).find('th').eq(4).should('contain', '29')
        cy.get('.col-md-7 tbody').find('tr').eq(2).find('th').eq(4).should('contain', '17')
    })
})

function addVehicles() {
    addOneVehicle('5', '26', '50')
    addOneVehicle('19', '8', '200')
    addOneVehicle('27', '2', '150')
}

function addVehicles2() {
    addOneVehicle('5', '26', '50')
    addOneVehicle('5', '26', '80')
    addOneVehicle('5', '26', '80')
}

function addOneVehicle(origin, destination, time) {
    cy.get('form[name="addVehicle"]').within(() => {
        cy.get('input[name="origin"]').clear().type(origin)
        cy.get('input[name="destination"]').clear().type(destination)
        cy.get('input[name="time"]').clear().type(time)
        cy.get('button').click()
    })
    cy.get('.modal-footer').find('button').click()
}